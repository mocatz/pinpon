import { writable } from 'svelte/store';

export const _playersScore = writable([0, 0]);
export const _playersName = writable([]);
export const _matchEnd = writable(false);

export const _playerList = writable([]);

// -- Match rules
export const _serviceNb = writable(2);
export const _playerNb = writable(2);
export const _scoreToWin = writable(11);
export const _diffScore = writable(2);

// -- Match records
/**
 * MODEL
 * {
    start: '2021-12-19T13:07:22.344Z', // --> Date Iso
    end: '2021-12-19T13:07:32.344Z', // --> Date Iso
    result: [
      {
        player: 'toto', // --> String
        score: 11 // --> Number
      },
      {
        player: 'tata', // --> String
        score: 7 // --> Number
      },
    ]
  },
 */
export const _matchCollection = writable([]);

// -- LOCAL STORAGE MANAGEMENT
export const _autoSaveFile = writable(true);
